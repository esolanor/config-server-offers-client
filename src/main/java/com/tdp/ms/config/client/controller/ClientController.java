package com.tdp.ms.config.client.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Edgar Solano Rojas
 * @Datecreation: 26/01/2022 14:49
 * @FileName: ClientController.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: description details
 */
@RestController
@RefreshScope
public class ClientController {

    @Value("${message}")
    private String message;

    private final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);


    @GetMapping(value = "/consult")
    public String get() {
        return message;
    }
}
