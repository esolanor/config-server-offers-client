package com.tdp.ms.config.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigServerOffersClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServerOffersClientApplication.class, args);
	}

}
